using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ripple.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ripple.Core.Types;
using Ripple.Core.Util;

namespace Ripple.Core.Tests
{
    [TestClass]
    public class AmountValueTests
    { 

        [TestMethod]
        public void IllegalAmountTest()
        {
            var thatDamnOffer = "1000000000000000100";
            var val = AmountValue.FromString(thatDamnOffer, native: true);
            Assert.AreEqual(thatDamnOffer, val.ToString());
        }
 
       
        public static void Normalize(ref string v)
        {
            v = v.Replace(",", "");
        }
    }
}
